var FormContact = (function () {

    function bindEvents($fContact) {
        $fContact.ibValidate(
            {
                name: {
                    events: ['blur', 'change'],
                    validateTypes: {
                        required: true
                    }
                },
                email: {
                    events: ['blur', 'keyup', 'change'],  // default
                    validateTypes: {
                        required: false,
                        email: true
                    }
                },
                phone: {
                    validateTypes: {
                        minLength: 7
                    }
                },
                message: {
                    validateTypes: {
                        maxLength: 50
                    }
                },
                password1: {
                    validateTypes: {
                        required: true,
                        minLength: 6,
                        equal: 'password2'
                    }
                },
                password2: {
                    validateTypes: {
                        required: true,
                        equalWith: 'password1'
                    }
                },
                gender: {
                    events: ['change'],
                    validateTypes: {
                        selectRequired: true
                    }
                },
                agreement: {
                    events: ['change'],
                    validateTypes: {
                        checked: true
                    }
                }
            }, {
                changeClassOnField: true,   // boolean: (true)  toggle ._error-* on field
                changeClassOnParent: true,  // boolean: (false) toggle ._error-* on parent
                parentSelector: 'label',    // jQuery Selector: ('label') Advice: use class names
                bindValidationEventImmediately: true,    // boolean: (false)
                submitButtonSelector: 'button',  // jQuery Selector: ('.js-submit')
                fieldValidateEventsArr: ['blur', 'keyup', 'change'], // events: (['blur', 'keyup', 'change'])

                submit: function () {
                    console.log('Add ajax here');
                }
            });

    }

    return {
        init: function ($fContact) {
            console.log('FormContact.init');
            bindEvents($fContact);
        }
    };

})();
