(function () {
    'use strict';

    // todo: add form object with all data about validation
    // todo: add Date validation
    // todo: add Pass regexp

    $.fn.ibValidate = function(rules, options) {

        this.settings = {
            changeClassOnField: true,   // boolean: (true)  toggle ._error-* on field
            changeClassOnParent: false,  // boolean: (false)  toggle ._error-* on parent
            parentSelector: 'label',    // jQuery Selector: ('label') Advice: use class names
            bindValidationEventImmediately: false,    // boolean: (false)
            submitButtonSelector: '.js-submit',  // jQuery Selector: ('.js-submit')
            fieldValidateEventsArr: ['blur', 'keyup', 'change'], // events: (['blur', 'keyup', 'change'])
            emailRegExp: /^([\w\-]+(?:\.[\w\-]+)*)@((?:[\w\-]+\.)*\w[\w\-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i,   // min possible x@x.xx

            // callbacks
            submit: function () {
                alert('Replace it with your AJAX request!')
            },
            invalid: function () {
                console.log('Invalid form callback');
            }
        };

        $.extend(this.settings, options);


        var that = this,
            $form = $(this),
            fieldNamesArr = Object.keys(rules),

            // flags
            isNotBindValidationEvents = true;

        function removeErrorClass($field, suffix) {
            if (that.settings.changeClassOnField) {
                $field.removeClass('_error-' + suffix);
            }
            if (that.settings.changeClassOnParent) {
                $field.parents(that.settings.parentSelector).removeClass('_error-' + suffix);
            }
        }
        function addErrorClass($field, suffix) {
            if (that.settings.changeClassOnField) {
                $field.addClass('_error-' + suffix);
            }
            if (that.settings.changeClassOnParent) {
                $field.parents(that.settings.parentSelector).addClass('_error-' + suffix);
            }
        }

        function checkRequired($field, currentValue, check) {
            if (check === true) {
                currentValue.length > 0 ? removeErrorClass($field, 'required') : addErrorClass($field, 'required');
            }
        }
        function checkEmail($field, currentValue, check, validateObj) {
            if (check === true) {
                if (validateObj.required === true || currentValue.length > 0) {
                    currentValue.length > 2 && isEmail(currentValue) // 2: magic number: because `q@w` this is a valid email name
                        ? removeErrorClass($field, 'email')
                        : addErrorClass($field, 'email');
                } else {
                    removeErrorClass($field, 'email');
                }
            }
        }
        function checkMinLength($field, currentValue, length, validateObj) {
            if (isNumeric(length)) {
                if (validateObj.required === true || currentValue.length > 0) {
                    currentValue.length >= length
                        ? removeErrorClass($field, 'min-length')
                        : addErrorClass($field, 'min-length');
                } else {
                    removeErrorClass($field, 'min-length');
                }
            }
        }
        function checkMaxLength($field, currentValue, length, validateObj) {
            if (isNumeric(length)) {
                if (validateObj.required === true || currentValue.length > 0) {
                    currentValue.length <= length
                        ? removeErrorClass($field, 'max-length')
                        : addErrorClass($field, 'max-length');
                } else {
                    removeErrorClass($field, 'max-length');
                }
            }
        }
        function checkChecked($field, value) {
            if (value === true) {
                if ($field.is(':checked')) {
                    removeErrorClass($field, 'checked');
                } else {
                    addErrorClass($field, 'checked');
                }
            }
        }
        function checkSelect($field, currentValue, value) {
            if (value === true) {
                if (currentValue !== '0' && currentValue !== null && currentValue !== false && currentValue !== undefined) {
                    removeErrorClass($field, 'select-required');
                    $field.parents('.jq-selectbox').removeClass('_error-select-required');
                } else {
                    addErrorClass($field, 'select-required');
                    $field.parents('.jq-selectbox').addClass('_error-select-required');
                }
            }
        }
        function checkEqual(value) {
            var $equalWith;
            if (value.length) {
                $equalWith = $form.find('[name="' + value + '"]');
                if ($equalWith.length) {
                    $equalWith.trigger('change', 'blur', 'keyup');
                }
            }
        }
        function checkEqualWith($field, currentValue, value) {
            var $equalWith;
            if (value.length) {
                $equalWith = $form.find('[name="' + value + '"]');
                if ($equalWith.length) {
                    if (currentValue === $equalWith.val()) {
                        removeErrorClass($field, 'equal');
                    } else {
                        addErrorClass($field, 'equal');
                    }
                }
            }
        }

        function isNumeric(value) {
            return !isNaN(parseFloat(value)) && isFinite(value);
        }
        function isEmail(currentValue) {
            return that.settings.emailRegExp.test(currentValue);
        }

        function validate(fieldName, validateObj) {
            var $field = $form.find('[name="' + fieldName + '"]'),
                currentValue = $field.val();
            $.each( validateObj, function( key, value ) {
                switch (key) {
                    case 'required':
                        checkRequired($field, currentValue, value);
                        break;
                    case 'email':
                        checkEmail($field, currentValue, value, validateObj);
                        break;
                    case 'minLength':
                        checkMinLength($field, currentValue, value, validateObj);
                        break;
                    case 'maxLength':
                        checkMaxLength($field, currentValue, value, validateObj);
                        break;
                    case 'checked':
                        checkChecked($field, value);
                        break;
                    case 'selectRequired':
                        checkSelect($field, currentValue, value);
                        break;
                    case 'equal':
                        checkEqual(value);
                        break;
                    case 'equalWith':
                        checkEqualWith($field, currentValue, value);
                        break;
                    default:
                        alert('Wrong validation type in ' + fieldName + ' field object!');
                }
            });
        }
        function validateAll() {
            for (var i = 0, maxI = fieldNamesArr.length; i < maxI; i += 1) {
                var validateObj = rules[fieldNamesArr[i]].validateTypes;
                if (typeof(validateObj) === 'object' && !Array.isArray(validateObj) && validateObj !== null) {
                    validate(fieldNamesArr[i], validateObj);
                }
            }
        }

        function isFormValid() {
            return $form.find('[class*="_error-"]').length === 0;
        }

        function bindValidationEvents() {
            var fieldSelector, eventsStr, eventsArr;
            for (var i = 0, maxI = fieldNamesArr.length; i < maxI; i += 1) {
                eventsArr = rules[fieldNamesArr[i]].events;
                eventsArr = Array.isArray(eventsArr) && eventsArr.length ? eventsArr : that.settings.fieldValidateEventsArr;
                eventsStr = eventsArr.join('.validate' + fieldNamesArr[i] + ' ') + '.validate' + fieldNamesArr[i];
                fieldSelector = '[name="' + fieldNamesArr[i] + '"]';
                $form.on(eventsStr, fieldSelector, { name: fieldNamesArr[i] }, function (e) {
                    var validateObj = rules[e.data.name].validateTypes;
                    if (typeof(validateObj) === 'object' && !Array.isArray(validateObj) && validateObj !== null) {
                        validate(e.data.name, validateObj);
                    }
                });
            }
        }
        function bindEvents() {
            $form
                .on('click.formSubmit', that.settings.submitButtonSelector, function (e) {
                    e.preventDefault();
                    e.stopPropagation();
                    if (isNotBindValidationEvents) {
                        bindValidationEvents();
                        isNotBindValidationEvents = false;
                    }
                    validateAll();
                    if (isFormValid()) {
                        that.settings.submit();
                    } else {
                        that.settings.invalid();
                    }
                });
        }

        function init() {
            bindEvents();

            that.settings.bindValidationEventImmediately && bindValidationEvents();
        }

        init();
    };

    //var Validation = (function () {
    //
    //    return {
    //        isNum: function (obj) {
    //            var re = /^\d+$/;
    //            if (re.test(obj.val())) {
    //                obj.removeClass('_error');
    //                return true;
    //            } else {
    //                obj.addClass('_error');
    //                return false;
    //            }
    //        },
    //        isEmail: function (obj) {
    //            var re = /^([\w\-]+(?:\.[\w\-]+)*)@((?:[\w\-]+\.)*\w[\w\-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
    //            if (re.test(obj.val())) {
    //                obj.removeClass('_error');
    //                return true;
    //            } else {
    //                obj.addClass('_error');
    //                return false;
    //            }
    //        },
    //        isIdentical: function (obj1, obj2) {
    //            if (obj1.val().length && obj1.val() == obj2.val()) {
    //                obj2.removeClass('_error');
    //                return true;
    //            } else {
    //                obj2.addClass('_error');
    //                return false;
    //            }
    //        }
    //    }
    //
    //}());

}());