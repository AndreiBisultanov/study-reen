(function () {
    'use strict';

//import("../components/forms/f-contact/f-contact.js");

    var App = (function (FormContact) {
        var $document = $(document),
            $window = $(window),
            $html = $document.find('html'),
            $body = $html.find('body'),
            $wrapper = $body.find('.wrapper'),

            // modul-detect
            $fContact = $body.find('.f-contact');

        return {
            init: function () {
                var style = 'padding: 5px 10px; background: linear-gradient(gold, orangered); font: 1.3rem Arial, sans-serif; color: white;';
                console.group('%c%s', style, 'App Initialization');
                console.log('App.init');

                $fContact.length && FormContact.init($fContact);

                console.groupEnd();
            }
        };
    }(
        FormContact));

    $(document).ready(function () {
        App.init();
    });

}());

/* $(document).ready(function () {

        // select
        $('select').styler();

        // file
         $fCV.on('change.toggleCommentForm', 'input[type="file"]', function () {
             var $that = $(this),
             $fileName = $that.siblings('.f-default__file-name');
             var file = $that.get(0).files[0];
             file ? $fileName.text(file.name) : $fileName.text($fileName.data('placeholder'));
         });


         // forms
        $('.js-num-only').on('keydown.onlyNumbers', function (e) {
            if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190, 116, 109, 189]) !== -1 || (e.keyCode === 65 && e.ctrlKey === true || e.keyCode === 86 && e.ctrlKey === true || e.keyCode === 67 && e.ctrlKey === true) || (e.keyCode >= 35 && e.keyCode <= 39)) {
                return;
            }
            if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                e.preventDefault();
            }
        });

        // print
        $('.js-print').on('click', function (e) {
            e.preventDefault();
            window.print();
        });

        // sliders
        $('.owl-main').owlCarousel({
            loop: true,
            margin: 0,
            dots: true,
            nav: true,
            navText: ['', ''],
            items: 1
        });

        var $sync1 = $('.owl-sync-1'),
            $sync2 = $('.owl-sync-2'),
            duration = 300,
            flag = false;

        initSyncSlider($sync1, $sync2, 3);
        function initSyncSlider($sync1, $sync2, thumbs) {
            $sync1
                .owlCarousel({
                    items: 1,
                    margin: 0,
                    navText: ['', ''],
                    nav: false,
                    dots: false,
                    mouseDrag: true,
                    touchDrag: true
                })
                .on('changed.owl.carousel', function (e) {
                    if (!flag) {
                        flag = true;
                        $sync2.trigger('to.owl.carousel', [e.item.index, duration, true]);
                        flag = false;
                        $sync2.find('.owl-item').removeClass('_active');
                        $sync2.find('.owl-item').eq(e.item.index).addClass('_active');
                    }
                });
            $sync2
                .owlCarousel({
                    margin: 0,
                    items: thumbs,
                    navText: ['', ''],
                    nav: true,
                    dots: false,
                    center: false,
                    navRewind: false,
                    mouseDrag: true,
                    touchDrag: true
                })
                .on('click', '.owl-item', function () {
                    $sync2.find('.owl-item').removeClass('_active');
                    $sync2.find('.owl-item').eq($(this).index()).addClass('_active');
                    $sync1.trigger('to.owl.carousel', [$(this).index(), duration, true]);
                })
                .on('changed.owl.carousel', function (e) {
                    if (!flag) {
                        flag = true;
                        $sync2.find('.owl-item').removeClass('_active');
                        $sync2.find('.owl-item').eq(e.item.index).addClass('_active');
                        $sync1.trigger('to.owl.carousel', [e.item.index, duration, true]);
                        flag = false;
                    }
                });
            $sync2.find('.owl-item.active').first().addClass('_active');
        }

        // maps contact
        var map_center_coords = [53.900, 27.550],
            map_zoom = 11,
            map;

        if ($('#b-map').length) {
            ymaps.ready(init);
        }

        function init() {
            var map = new ymaps.Map('b-map', {
                    center: [map_center_coords[0], map_center_coords[1]],
                    zoom: map_zoom,
                    controls: []
                }),
                MyBalloonLayout = ymaps.templateLayoutFactory.createClass(
                    '<div class="popover top">' +
                    '<div class="arrow"></div>' +
                    '<div class="popover-inner">' +
                    '$[[options.contentLayout observeSize minWidth=220 maxWidth=300 maxHeight=350]]' +
                    '</div>' +
                    '</div>', {
                        build: function () {
                            this.constructor.superclass.build.call(this);
                            this._$element = $('.popover', this.getParentElement());
                            this.applyElementOffset();
                            this._$element.find('.b-balloon__close-about')
                                .on('click', $.proxy(this.hideAbout, this));
                            this._$element.find('.b-balloon__inner')
                                .on('click', $.proxy(this.showAbout, this));
                        },
                        applyElementOffset: function () {
                            this._$element.css({
                                left: 0,
                                top: 0
                            });
                        },
                        hideAbout: function (e) {
                            e.preventDefault();
                            $('.b-balloon__inner-about').hide();
                            $('.b-balloon__inner').show();
                        },
                        showAbout: function () {
                            $('.b-balloon__inner-about').show();
                            $('.b-balloon__inner').hide();
                        }
                    }),
                MyBalloonContentLayout = ymaps.templateLayoutFactory.createClass(
                    '<div class="b-balloon">' +
                    '<div class="b-balloon__inner">$[properties.balloonContent]</div>' +
                    '</div>'
                ),
                ballonOptions = {
                    iconLayout: 'default#image',
                    iconImageHref: '../assets/i/pointer.png',
                    iconImageSize: [34, 48],
                    iconImageOffset: [-3, -42],
                    balloonShadow: false,
                    balloonLayout: MyBalloonLayout,
                    balloonContentLayout: MyBalloonContentLayout,
                    balloonPanelMaxMapArea: 0,
                    hideIconOnBalloonOpen: false,
                    balloonOffset: [25, -35]
                };
            shopsArr.forEach(function(place, i, arr) {
                map.geoObjects.add(new ymaps.Placemark([place.lng, place.lat], {
                    balloonContent: place.balloonContent
                }, ballonOptions));
            });

            //$('.b-map__city').on('click', function (e) {
            //    e.preventDefault();
            //    var $that = $(this);
            //    $that.addClass('_active').siblings().removeClass('_active');
            //    map.panTo([$that.data('lng'), $that.data('lat')], {
            //        flying: 3
            //    });
            //});
        }

        // maps shops
        if ($('.s-shops-b').length) {
            ymaps.ready(initShops);
        }

        var myMapShops,
            myPlacemarkShops;

        function initShops() {
            for (var i = 0; i < shopsArr.length; i += 1) {
                myMapShops = new ymaps.Map(shopsArr[i].id, {
                    center: [shopsArr[i].lng, shopsArr[i].lat],
                    zoom: 14,
                    controls: []
                });
                myPlacemarkShops = new ymaps.Placemark([shopsArr[i].lng, shopsArr[i].lat], {
                    hintContent: shopsArr[i].hitContent,
                    balloonContent: shopsArr[i].balloonContent
                },{
                    iconLayout: 'default#image',
                    iconImageHref: '../assets/i/pointer.png',
                    iconImageSize: [34, 48],
                    iconImageOffset: [-3, -42]
                });
                myMapShops.geoObjects.add(myPlacemarkShops);
            }
        }

        // etc
        $('._phone-mask').mask('+375 (99)-999-99-99');

    });*/
