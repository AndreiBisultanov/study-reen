'use strict';

var gulp = require('gulp'),
    jade = require('gulp-jade'),
    // todo gulp-jade-find-affected будет компилить только измененные файлы jade, возможно неактуально с Gulp4, необходим gulp-watch
    stylus = require('gulp-stylus'),
    sourceMaps = require('gulp-sourcemaps'),
    gulpImports = require('gulp-imports'),
    autoPrefixer = require('gulp-autoprefixer'),
    imageMin = require('gulp-imagemin'),
    imageMinMoz = require('imagemin'),
    imageMinMozJpeg = require('imagemin-mozjpeg'),
    newer = require('gulp-newer'),
    plumber = require('gulp-plumber'),
    prettify = require('gulp-html-prettify'),
    browserSync = require('browser-sync').create(),
    spriteSmith = require('gulp.spritesmith'),
    htmlRoot = '/html',
    imagesFolder = 'img';

gulp.task('sprite', function () {
  gulp.src('./assets/' + imagesFolder + '/sprite/*.png')
    .pipe(spriteSmith({
      padding: 20,
      imgPath: '../img/sprite.png',
      imgName: 'sprite.png',
      cssName: 'sprite.styl'
    }))
    .pipe(gulp.dest('./assets/' + imagesFolder + '/'));
});
gulp.task('images', function () {
    gulp.src(['./assets/' + imagesFolder + '/*.png', './assets/' + imagesFolder + '/*.gif', './assets/' + imagesFolder + '/*.svg'])
        .pipe(plumber())
        .pipe(newer('./public/assets/' + imagesFolder + '/'))
        .pipe(imageMin({
            progressive: true,
            interlaced: true,
            svgoPlugins: [{removeViewBox: false}, {removeUselessStrokeAndFill: false}]
        }))
        .pipe(gulp.dest('./public/assets/' + imagesFolder + '/'));
});
gulp.task('mozJpeg', function () {
    imageMinMoz(['./assets/' + imagesFolder + '/*.jpg'], './public/assets/' + imagesFolder + '/', {use: [imageMinMozJpeg({quality: 95})]}).then(() => {
        console.log('Images optimized');
    });
});

gulp.task('styles', function () {
    gulp.src('./assets/common/stylus/common.styl')
        .pipe(plumber())
        .pipe(sourceMaps.init())
        .pipe(stylus({
            compress: false
        }))
        .pipe(autoPrefixer({
            browsers: ['> 1%', 'last 4 versions', 'Firefox ESR', 'Opera 12.1'],
            cascade: false
        }))
        .pipe(sourceMaps.write('.'))
        .pipe(gulp.dest('./public/assets/css'));
});

gulp.task('jade', function () {
    gulp.src('./assets/views/*.jade')
        .pipe(plumber())
        .pipe(jade({
            pretty: true
        }))
        .pipe(prettify({indent_char: ' ', indent_size: 4}))
        .pipe(gulp.dest('./public' + htmlRoot));
});

gulp.task('scripts', function () {
    gulp.src('./assets/js/**/*.js')
        .pipe(plumber())
        .pipe(newer('./assets/js/**/*'))
        .pipe(gulpImports())
        .pipe(gulp.dest('./public/assets/js/'));
});

gulp.task('fonts', function () {
    gulp.src('./assets/fonts/**/*')
        .pipe(plumber())
        .pipe(newer('./assets/fonts/**/*'))
        .pipe(gulp.dest('./public/assets/fonts'));
});

gulp.task('bs', function () {
    browserSync.init({
        server: './public',
        directory: true,
        open: false,
        notification: false,
        reloadOnRestart: false,
        ghostMode: true,
        startPath: 'html/index.html'
    });
    //browserSync.watch('./public/**/*.*').on('change', browserSync.reload);
});

gulp.task('watch', function () {
    gulp.watch(['./assets/' + imagesFolder + '/sprite.styl', 'assets/common/stylus/**/*.styl', './assets/components/**/*.styl'], ['styles']);
    gulp.watch(['./assets/common/jade/**/*.jade', './assets/components/**/*.jade', './assets/views/*.jade'], ['jade']);
    gulp.watch(['./assets/js/**/*.js', './assets/components/**/*.js'], ['scripts']);
    gulp.watch('./assets/' + imagesFolder + '/sprite/*', ['sprite']);
    gulp.watch(['./assets/' + imagesFolder + '/*.png', './assets/' + imagesFolder + '/*.gif', './assets/' + imagesFolder + '/*.svg'], ['images']);
    gulp.watch('./assets/' + imagesFolder + '/*.jpg', ['mozJpeg']);
    gulp.watch('./assets/fonts/**/*', ['fonts']);
});

gulp.task('build', ['sprite', 'styles', 'jade', 'scripts', 'images', 'mozJpeg', 'fonts']);
gulp.task('default', ['build', 'watch', 'bs']);

// todo gulp 4 since
// todo update info file
// todo add ES2015
