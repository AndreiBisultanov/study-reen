# Project Template
**Шаблон проекта для быстрого старта.**

## Старт проекта

* Установи `gulp` глобально:

```bash
npm i -g gulp
```

* Установи зависимости:

```
npm i
```

* Запусти Gulp.js:

```
gulp
```

* Открой в браузере [`http://localhost:3000/`](http://localhost:3000/).

## Команды для запуска с Gulp.js

* Запуск Gulp с отслеживанием изменений:

```
gulp
```

* Сборка в папку `public`:

```
gulp build
```

## Структура папок и файлов

```
├── assets/                         # Исходники
│   ├── fonts/                     # Шрифты
│   ├── img/                       # Изображения
│   │   └── sprite/
│   ├── js/                        # JavaScript
│   │   ├── vendor/                # Vendors
│   │   │   └── *.js              # Vendor js
│   │   └── main.js                # Главный JavaScript файл
│   ├── stylus/                     # Stylus
│   │   ├── blocks/
│   │   ├── etc/
│   │   ├── forms/
│   │   ├── lists/
│   │   ├── menus/
│   │   ├── sections/
│   │   ├── settings/              # Настройки
│   │   │   ├── default.styl      # Стандартные стили, информация по префиксам
│   │   │   ├── fonts.styl	      # подключчение шрифтов
│   │   │   ├── mixin.styl	      # Миксины
│   │   │   ├── optimize.styl	  # Сброс настроек и оптимизация пол старые браузеры
│   │   │   └── variables.styl    # Переменные
│   │   ├── vendor/
│   │   └── common.styl           # Главный Stylus файл
│   └── template/                  # Jade
│        ├── blocks/
│        │   └── *.jade
│        ├── etc/
│        ├── forms/
│        ├── lists/
│        ├── menus/
│        ├── sections/
│        └── index.jade            # Страницы
├── public/                         # Сборка
│   ├── assets/                    # Подключаемые ресурсы
│   │   ├── fonts/                # Шрифты
│   │   ├── img/                  # Изображения
│   │   ├── js/                   # JavaScript
│   │   └── css/                  # Стили
│   └── html/                      # HTML
│        ├── index.html            # Страница
│        └── another.html          # Другая страница
├── .editorconfig                   # Конфигурация настроек редактора кода
├── .gitignore                      # Список исключённых файлов из Git
├── gulpfile.js                     # Файл для запуска Gulp.js
├── ib-jscs.json                    # Конфигурация проверки JavaScript в jscs
├── package.json                    # Список модулей и прочей информации
├── readme.md                       # Документация шаблона
└── tools.json                      # Использованные плагины
```
